package com.yato.mapper.cache;

import com.yato.mapper.cache.RedisMapper;
import com.yato.entity.Seckill;
import com.yato.mapper.ISeckillMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/spring-mapper.xml"})
public class RedisMapperTest {

    private Long id = 1000L;
    //    @Autowired
    private RedisMapper redisMapper = new RedisMapper();
    @Autowired
    private ISeckillMapper seckillMapper;

    @Test
    public void getSeckill() {
        System.out.println(redisMapper.getSeckill(1000L));
    }

    @Test
    public void putSeckill() {
        final Seckill seckill = seckillMapper.queryById(id);
        System.out.println(redisMapper.putSeckill(seckill));
    }
}