package com.yato.mapper.cache;

import com.dyuproject.protostuff.LinkedBuffer;
import com.dyuproject.protostuff.ProtostuffIOUtil;
import com.yato.entity.Seckill;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import com.dyuproject.protostuff.runtime.RuntimeSchema;
//@Component
public class RedisMapper {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final JedisPool jedisPool;
    private RuntimeSchema<Seckill> schema = RuntimeSchema.createFrom(Seckill.class);

    public RedisMapper(){
        this("192.168.75.129", 6379);
    }

    public RedisMapper(String ip, int port) {
        this.jedisPool = new JedisPool(ip, port);
    }

    /**
     * 根据 seckillId 从缓存中获取对应对象
     * @param seckillId
     * @return
     */
    public Seckill getSeckill(Long seckillId){
        /*
        * jedis 内部没有实现序列化操作，所以需要我们自己序列化
        *
        * 序列化有很多方式，比如 java 提供的 实现 serializable 接口，或者其他开源的 jar 包
        *
        * 这里使用 protostuff 来完成序列化（序列化速度和序列化后对象的占用内存都比 java 内置的序列化方式优秀得多）
        *       protostuff 要求序列化的对象必须是一个 pojo（要有 setter and getter）
        *
        * get -> byte[] -> 反序列化 ->Object(Seckill)
        * */
        try (
            Jedis jedis = jedisPool.getResource();
        ) {
            String key = "seckill:" + seckillId;
            // 从缓存中查找对应 SeckillId 的对象（获取存储的对象序列化后的字节码）
            byte[] bytes = jedis.get(key.getBytes());
            // 如果 bytes 不为空，则缓存命中
            if (null != bytes){
                // 构建一个空的目标对象（Seckill），让 protostuff 利用 bytes 反序列化到这个空对象
                Seckill seckill = schema.newMessage();
                ProtostuffIOUtil.mergeFrom(bytes, seckill, schema);
                // 此时 seckill 已经被赋值，返回即可
                return  seckill;
            }

        } catch (Exception e){
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    /**
     * 把一个 seckill 对象放入到 redis 缓存中
     * @param seckill
     * @return
     */
    public String putSeckill(Seckill seckill){
        // set Object(Seckill) -> 序列化 -> byte[]
        try (Jedis jedis = jedisPool.getResource()){
            String key = "seckill:" + seckill.getSeckillId();
            // 需要使用 protostuff 中的缓存器，如果对象较大，可以做一个缓冲的过程
            final byte[] bytes = ProtostuffIOUtil.toByteArray(seckill, schema,
                    LinkedBuffer.allocate(LinkedBuffer.DEFAULT_BUFFER_SIZE));
            // 超时缓存 // 一个小时
            int timeout = 60 * 60;
            // setex 返回值是对应语句执行后的返回值结果
            final String result = jedis.setex(key.getBytes(), timeout, bytes);
            return result;
        }
    }


}
